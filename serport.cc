// -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil -*-

#include "serport.hh"

#include <fcntl.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <syslog.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

//#include <sys/signal.h>
   
using namespace std;

// ====================================================================
//
//
//
// ====================================================================

  
serport::serport(string dev, int brate)
    : device(dev), baudrate(brate), fd(-1)
{

  
    bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
    //newtio.c_cflag = baudrate |  CS8 | CLOCAL | CREAD;
    newtio.c_cflag = baudrate |  CS8 | CREAD;
    newtio.c_iflag = IGNPAR | ICRNL;
    newtio.c_oflag = 0;
    newtio.c_lflag = ICANON;
    newtio.c_cc[VINTR]    = 0;     /* Ctrl-c */ 
    newtio.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
    newtio.c_cc[VERASE]   = 0;     /* del */
    newtio.c_cc[VKILL]    = 0;     /* @ */
    newtio.c_cc[VEOF]     = 4;     /* Ctrl-d */
    newtio.c_cc[VTIME]    = 0;     /* inter-character timer unused */
    newtio.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
    newtio.c_cc[VSWTC]    = 0;     /* '\0' */
    newtio.c_cc[VSTART]   = 0;     /* Ctrl-q */ 
    newtio.c_cc[VSTOP]    = 0;     /* Ctrl-s */
    newtio.c_cc[VSUSP]    = 0;     /* Ctrl-z */
    newtio.c_cc[VEOL]     = 0;     /* '\0' */
    newtio.c_cc[VREPRINT] = 0;     /* Ctrl-r */
    newtio.c_cc[VDISCARD] = 0;     /* Ctrl-u */
    newtio.c_cc[VWERASE]  = 0;     /* Ctrl-w */
    newtio.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
    newtio.c_cc[VEOL2]    = 0;     /* '\0' */

}


// ====================================================================
//
//
//
// ====================================================================

void serport::open_dev()
{
    fd = open(device.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK ); 
    if (fd <0) {
        cout << 42 << endl;
        syslog(LOG_ERR, "error opening %s: %s",
               device.c_str(), strerror(errno)); 
        exit(-1); 
    }
    
    tcgetattr(fd,&oldtio); /* save current serial port settings */
    tcflush(fd, TCIFLUSH);
    tcsetattr(fd,TCSANOW,&newtio);
}

// ====================================================================
//
//
//
// ====================================================================

void serport::close_dev()
{
    tcsetattr(fd,TCSANOW,&oldtio);
    close(fd);
    fd = -1;
}


// ====================================================================
//
//
//
// ====================================================================

bool serport::check_complete(char* buf, int len)
{
    if (len<0) return false;

    int count = 0;
    for (int i=0; i<len; i++) {
        if (buf[i]=='\n') count++;
    }
    
    if ( count >= 4 )
        return true;
    else
        return false;
}

void serport::send(const char* format, ...)

{
    // handle varargs
    // --------------

    char command[100];

    va_list ap;
    
    va_start(ap, format); 
    vsprintf (command,format, ap);
    //perror (command);
    va_end(ap);

    
    // open device if necessary
    // ------------------------
    
    bool fd_open = is_open();
    if (!fd_open) open_dev();


    // write single characters at a time
    // ---------------------------------

    //cout << "sending:" << hex << setfill('0') << flush;
    for (int i=0; i<strlen(command); i++) {
        //cout << " " << setw(2) << int(command[i]) << flush;
        write(fd, command+i, 1);
        usleep(3000);
    }
    //cout << dec << setfill(' ') << endl;

    
    // clean up
    // --------

    if (!fd_open) close_dev();
}


// ====================================================================
//
//
//
// ====================================================================

string serport::receive()
{

    // open device if necessary
    // ------------------------
    bool fd_open = is_open();
    if (!fd_open) open_dev();


    // receive data
    // ------------
    static char buf[256];
    memset(buf,0,256);

    int nbytes=0, nbytestot=0, nreads=0;

    int read_delay = 10000;
    
    // wait for first data to appear
    for (int i=0; i<100; i++) {
        usleep(read_delay);

        nbytes = read(fd,buf+nbytestot,256-nbytestot);

        
        if (nbytes != -1) {
            // we read data
            nbytestot += nbytes;
            nreads++;
        } else if (nbytestot > 0) {
            // we have read some data, but there is nothing now
            break;
        }
        
        // we have not read data yet, continue trying...

    }    

//    for (int i=0; i<100; i++) {
//        usleep(5000);
//    
//        nbytes = read(fd,buf+nbytestot,256-nbytestot);
//
//        if (nbytes >= 0) {
//            nbytestot += nbytes;
//            nreads++;
//        }
//        
//        //if (nbytes >= 0) {
//        //    cout << setw(6) << i << " - " 
//        //         << "received " << setw(3) << nbytestot
//        //         << " bytes in " << setw(3) << nreads << " attempts: ";
//        //    for (int i=0; i< nbytestot; i++) {
//        //        cout << " " << setw(2) << setfill('0') << hex 
//        //             << int(buf[i]) << dec << setfill(' ') 
//        //             << flush;
//        //    }
//        //    cout << endl;
//        //}
//
//        if (check_complete(buf, nbytestot)) {
//            break;
//        }
//    }
//            
    
    string error;
    switch (errno) {
    case EAGAIN: error = "EAGAIN"; break;
    case EBADF:  error = "EBADF";  break;
    case EFAULT: error = "EFAULT"; break;
    case EINTR:  error = "EINTR";  break;
    case EINVAL: error = "EINVAL"; break;
    case EIO:    error = "EIO";    break;
    case EISDIR: error = "EISDIR"; break;
    }

    
//     syslog(LOG_DEBUG, "received %d bytes in %d attempts: %s %s",
//            nbytestot, nreads, error.c_str(), buf);

    // optional debugging output
    if (0) {
    
        cout << "received " << setw(3) << nbytestot
             << " bytes in " << setw(3) << nreads << " attempts: ";
        for (int i=0; i< nbytestot; i++) {
            cout << " " << setw(2) << setfill('0') << hex 
                 << int(buf[i]) << dec << setfill(' ') 
                 << flush;
        }
        cout << endl;
    }


    // clean up
    // --------
    if (!fd_open) close_dev();

    return buf;
    
}



// ====================================================================
//
//  exec: send and receive data
//
// ====================================================================

string serport::exec(const char* format, ...)
{

    // handle varargs
    // --------------
    char command[100];

    va_list ap;
    
    va_start(ap, format); 
    vsprintf (command,format, ap);
    va_end(ap);


    // check if device is open
    // -----------------------

    bool fd_open = is_open();
    if (!fd_open) open_dev();


    // send the command
    // ----------------
    send(command); 


    // retrieve the answer
    // -------------------

    string result;
    do {
        usleep(5000);
        result = receive();
    } while (result.size()==0);

//     command[strlen(command)-1] = '\0';
//     syslog(LOG_DEBUG, "exec: sent \"%s\", received \"%s\"",
//            command, result.c_str());


    // clean up
    // --------
    if (!fd_open) close_dev();

    return result;
}



// ====================================================================
//
//   shortcuts for send(string) and exec(string)
//
// ====================================================================

void serport::send(string command)
{
    send(command.c_str());
}


string serport::exec(string command) 
{
    return exec(command.c_str());
}

