#ifndef SER2DIM_SERVICES_HH
#define SER2DIM_SERVICES_HH

#include <string>
#include "serport.hh"

#include <Poco/Timestamp.h>

class DimService;
class DimCommand;

// base class for services
class service
{
public:

  service(serport* device, std::string name, std::string cmd,
	  int intvl=1000000);

  std::string name() {return thename;}

  // query, read back and update the value
  virtual void update(int maxretries=1, bool checkinterval=true);

  // return the last read value as string
  std::string value_as_string() { return lastvalstr; }

  // parsing of the command:
  // possible results: succeed, fail, or require more data
  enum parse_status_t { good, fail, wait };
  virtual parse_status_t parse(std::string txt);

protected:

  parse_status_t receive_and_parse();
  
  serport* dev;
  std::string thename;
  std::string command;

  std::string lastvalstr;
  
  Poco::Timestamp lastupdate;
  Poco::Timestamp::Timestamp::TimeDiff interval;
  
};

class service_nhqfloat : public service
{
public:
  service_nhqfloat(serport* device,
		   std::string name,
		   std::string cmd,
		   int intvl=1000000);

  virtual service::parse_status_t parse(std::string txt);

  // convenient access to the float value
  float read() { update(); return val; } 
  float get() { return val; } 
  void set(float x);

protected:
  float val, valm, vale;
  std::string cmd;

  DimService* dimsvc;
};


#endif
