

#include <iostream>
#include <iomanip>
#include <string>
#include <list>

#include <cstring>
#include <unistd.h>

#include "serport.hh"
#include "services.hh"
#include "util.hh"

using namespace std;

void read_services(list<service>& services);

int main(int argc, char** argv)
{

  serport ser("/dev/ttyUSB0");//, B9660);
  ser.send("\r\n");
  usleep(50000);

  //cout << ser.exec("#\r\n") << endl;
  //cout << ser.exec("#\r\n") << endl;
  //cout << ser.exec("#\r\n") << endl;
  //cout << ser.exec("S1\r\n") << endl;

  
  list<service> services;

  services.push_back(service(&ser, "id",      "#\r\n"));

  //services.push_back(service(&ser, "status1", "S1\r\n", ""));
  //services.push_back(service(&ser, "Vmeas1",  "U1\r\n", ""));
  //services.push_back(service(&ser, "Imeas1",  "I1\r\n", ""));
  //services.push_back(service(&ser, "Vlimit1", "M1\r\n", ""));
  //services.push_back(service(&ser, "Ilimit1", "N1\r\n", ""));
  //services.push_back(service(&ser, "Vset1",   "D1\r\n", ""));
  //services.push_back(service(&ser, "Vramp1",  "V1\r\n", ""));

  service_nhqfloat svc_Itrip1(&ser, "Itrip1",  "L1");
  service_nhqfloat svc_Vset1(&ser, "Vset1",  "D1");
  
  services.push_back(svc_Itrip1);

  //services.push_back(service(&ser, "MStat1",  "T1\r\n", ""));

  //services.push_back(service(&ser, "status2", "S2\r\n", ""));
  //services.push_back(service(&ser, "Vmeas2",  "U2\r\n", ""));
  //services.push_back(service(&ser, "Imeas2",  "I2\r\n", ""));
  //services.push_back(service(&ser, "Vlimit2", "M2\r\n", ""));
  //services.push_back(service(&ser, "Ilimit2", "N2\r\n", ""));
  //services.push_back(service(&ser, "Vset2",   "D2\r\n", ""));
  //services.push_back(service(&ser, "Vramp2",  "V2\r\n", ""));
  //services.push_back(service(&ser, "Itrip2",  "L2\r\n", ""));
  //services.push_back(service(&ser, "MStat2",  "T2\r\n", ""));


  if (argc < 2) {
    cerr << "usage: nhq <command>" << endl << endl
	 << "   command:" << endl
	 << "      read   -   read all registers once" << endl
	 << "      loop   -   read all registers in an infinite loop" << endl
	 << endl;

    return -1;
  }
  
  if ( !strncmp(argv[1], "read", 4) ) {
    read_services(services);
    
  } else if ( !strncmp(argv[1], "loop", 4) ) {
    while(1) {
      read_services(services);
    }

  } else if ( !strncmp(argv[1], "on", 2) ) {

    svc_Itrip1.set(20e-7);
    
    cout << txt2hex(ser.exec("V1=5\r\n")) << endl;
    //cout << txt2hex(ser.exec("L1=0020\r\n")) << endl;
    cout << txt2hex(ser.exec("D1=0080\r\n")) << endl;
    cout << txt2hex(ser.exec("G1\r\n")) << endl;

  }
}

void read_services(list<service>& services)
{
  for ( list<service>::iterator svc=services.begin();
	svc!=services.end(); svc++) {

    svc->update();
    cout << setw(10) << svc->name() << ": "
	 << svc->value_as_string() << endl;
  }
}
  

